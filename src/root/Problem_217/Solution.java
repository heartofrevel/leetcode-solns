package root.Problem_217;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class Solution {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> numsSet = new HashSet<>(nums.length);
        for (int num : nums){
            if(numsSet.contains(num))
                return true;
            numsSet.add(num);
        }
        return false;
    }
}
