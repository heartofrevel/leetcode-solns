package root.Problem_1041;

class Solution {
    public boolean isRobotBounded(String instructions) {
        int[][] pds = new int[][]{{0,1}, {1,0}, {0,-1}, {-1,0}};
        int x=0,y=0;
        int cd = 0;

        for(char ch : instructions.toCharArray()){
            if(ch=='L'){
                cd = (cd+3)%4;
            }else if(ch=='R'){
                cd = (cd+1)%4;
            }else{
                x+=pds[cd][0];
                y+=pds[cd][1];
            }
        }

        return (x==0 && y==0) || (cd!=0);
    }
}