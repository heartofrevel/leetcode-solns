package root.Problem_557;

public class Solution {
    public String reverseWords(String s) {
        String[] words = s.split(" ");

        for(int k=0; k<words.length; k++ ){
            String word = words[k];
            int len = word.length();
            int i = 0, j=len-1;
            char[] charWords = word.toCharArray();
            while(i<j){
                char temp = charWords[i];
                charWords[i] = charWords[j];
                charWords[j] = temp;
                i++;
                j--;
            }
            words[k] = String.valueOf(charWords);
        }
        return String.join(" ", words);
    }
}