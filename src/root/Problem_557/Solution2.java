package root.Problem_557;

public class Solution2 {
    public String reverseWords(String s) {
        int i=0,j=0;
        char[] stringArray = s.toCharArray();
        int len = s.length();
        while(j<=len){
            if(j==len || stringArray[j]==' '){
                int m = i;
                int n = j-1;
                while(m<n){
                    char temp = stringArray[m];
                    stringArray[m] = stringArray[n];
                    stringArray[n] = temp;
                    m++;
                    n--;
                }
                j++;
                i=j;
            }else {
                j++;
            }
        }
        return String.valueOf(stringArray);
    }
}
