package root.Problem_1089;

public class Solution {
    public void duplicateZeros(int[] arr) {
        int len = arr.length;
        int[] tempArray = new int[len*2];
        int ti=0;
        int count=0;
        for(int i=0; i<len-count; i++){
           if(arr[i] == 0){
               tempArray[ti++] = 0;
               count++;
           }
           tempArray[ti++]=arr[i];
        }
        for(int i=0; i<len; i++){
            arr[i] = tempArray[i];
        }
    }
}
