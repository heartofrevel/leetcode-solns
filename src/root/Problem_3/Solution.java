package root.Problem_3;

import java.util.LinkedHashMap;
import java.util.Map;

public class Solution {
//    public int lengthOfLongestSubstring(String s) {
//        Map<Character, Integer> mapSubstring = new LinkedHashMap<>();
//        int len = s.length();
//        int maxLength = 0;
//        int length = 0;
//        int prevIndex = 0;
//        for(int i=0; i<len;i++){
//            char x = s.charAt(i);
//            if(mapSubstring.containsKey(x)){
//                if(maxLength<length) maxLength=length;
//                length = length - mapSubstring.get(x) + prevIndex +1;
//                prevIndex = mapSubstring.get(x);
//                mapSubstring.remove(x);
//                mapSubstring.put(x, i);
//            }else{
//                length++;
//                mapSubstring.put(x, i);
//            }
//        }
//        return maxLength;
//    }

    public int lengthOfLongestSubstring(String s) {
        int[] chars = new int[128];

        int left = 0;
        int right = 0;

        int res = 0;
        while (right < s.length()) {
            char r = s.charAt(right);
            chars[r]++;

            while (chars[r] > 1) {
                char l = s.charAt(left);
                chars[l]--;
                left++;
            }

            res = Math.max(res, right - left + 1);

            right++;
        }
        return res;
    }
}

//abcbca