package root.Problem_824;

import java.util.Collections;

public class Solution {
    public String toGoatLatin(String sentence) {
        char[] sArray = sentence.toCharArray();

        StringBuilder result = new StringBuilder();
        int i=0,j=0;
        int len = sentence.length();
        boolean isVowel = true;
        int wordCount = 0;
        char firstChar='a';
        while(j<len){
            char ch = sArray[j];
            if(i==j) {
                if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ||
                        ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
                    isVowel = true;
                    result.append(ch);
                }else{
                    firstChar = ch;
                    isVowel = false;
                }
                j++;
                continue;
            }
            if(ch == ' ' || j == len-1){
                wordCount++;
                if(j==len-1)
                    result.append(ch);
                if(!isVowel){
                    result.append(firstChar);
                }
                result.append("ma");
                result.append(String.join("", Collections.nCopies(wordCount, "a")));
                if(!(j==len-1))
                    result.append(' ');
                j++;
                i=j;
            }else{
                result.append(ch);
                j++;
            }
        }
        return result.toString();
    }
}