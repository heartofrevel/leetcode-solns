package root.Problem_2;

public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2){
        int carry = 0;
        ListNode result = new ListNode(0);
        ListNode current = result;
        while(l1!=null || l2!=null){
            int l1Val = (l1 != null) ? l1.val : 0;
            int l2Val = (l2 != null) ? l2.val : 0;
            int sum = l1Val+l2Val+carry;
            carry = sum/10;
            current.next = new ListNode(sum%10);
            current = current.next;
            if (l1 != null)
                l1 = l1.next;
            if(l2 != null)
                l2 = l2.next;
        }
        if(carry > 0)
            current.next = new ListNode(carry);
        return result.next;
    }
}




/**
 * Definition for singly-linked list.**/
class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
