package root.Problem_43;

public class Solution {
    /*
    public String multiply(String num1, String num2) {
        StringBuilder result = new StringBuilder("0");

        int indexNum2 = num2.length() - 1 ;

        while(indexNum2 >= 0){
            int x2 = indexNum2 >= 0 ? num2.charAt(indexNum2) - '0' : 0;
            int carry = 0;
            StringBuilder temp = new StringBuilder();
            int indexNum1 = num1.length() - 1 ;
            while(indexNum1 >= 0){
                int x1 = indexNum1 >= 0 ? num1.charAt(indexNum1) - '0' : 0;
                int mult = (x1*x2+carry)%10;
                carry = (x1*x2+carry)/10;
                temp.append(mult);
                indexNum1--;
            }
            if(carry != 0){
                temp.append(carry);
            }
            int pad = num2.length() - indexNum2 - 1;
            temp = temp.reverse();
            padString(temp, pad);
            result = new StringBuilder(addStrings(result.toString(), temp.toString()));
            indexNum2--;
        }
        if(onlyZeroes(result.toString())){
            return "0";
        }
        return result.toString();
    }
    */

    public String multiply(String num1, String num2){
        int len1 = num1.length();
        int len2 = num2.length();
        int result[] = new int[len1 + len2];
        for (int i = len1 - 1; i >= 0; i--){
            int n1 = num1.charAt(i) - '0';
            for (int j = len2 - 1; j >= 0; j--){
                int n2 = num2.charAt(j) - '0';
                int mul = n1 * n2;
                int sum = mul + result[i + j + 1];
                result[i + j] += sum / 10;
                result[i + j + 1] = sum % 10;
            }
        }
        StringBuilder finalResult = new StringBuilder();
        boolean foundNum=false;
        for(int i=0; i<result.length; i++){
            if(i==result.length-1 && result[i]==0){
                finalResult.append(0);
                continue;
            }
            if(result[i]==0 && !foundNum){
                continue;
            }
            foundNum=true;
            finalResult.append(result[i]);
        }
        return finalResult.toString();
    }
    /*
    public String addStrings(String num1, String num2) {
        StringBuilder result = new StringBuilder();
        int indexNum1 = num1.length() - 1 ;
        int indexNum2 = num2.length() - 1 ;
        int carry = 0;
        while(indexNum1 >= 0 || indexNum2 >= 0){
            int x1 = indexNum1 >= 0 ? num1.charAt(indexNum1) - '0' : 0;
            int x2 = indexNum2 >= 0 ? num2.charAt(indexNum2) - '0' : 0;
            int sum = (x1+x2+carry)%10;
            carry = (x1+x2+carry)/10;
            result.append(sum);
            indexNum1 --;
            indexNum2 --;
        }

        if(carry!=0){
            result.append(carry);
        }

        return result.reverse().toString();
    }

    public void padString(StringBuilder input, int length){
        for(int i=0; i<length; i++){
            input.append('0');
        }
    }

    public boolean onlyZeroes(String input){
        boolean result = true;
        for (char c : input.toCharArray()){
            if(c != '0'){
                return false;
            }
        }
        return result;
    }

    */
}
