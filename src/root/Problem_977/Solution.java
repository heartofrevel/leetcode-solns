package root.Problem_977;

public class Solution {
    public int[] sortedSquares(int[] nums) {
        int left = 0;
        int right = nums.length-1;
        int[] result = new int[nums.length];
        int num = 0;
        for(int i=right; i>=0; i--){
            if(Math.abs(nums[left]) > Math.abs(nums[right])){
                num = nums[left];
                left++;
            }else{
                num = nums[right];
                right--;
            }
            result[i] = num*num;
        }
        return result;
    }
}
