package root.Problem_415;

public class Solution {
    public String addStrings(String num1, String num2) {
        StringBuilder result = new StringBuilder();
        int indexNum1 = num1.length() - 1 ;
        int indexNum2 = num2.length() - 1 ;
        int carry = 0;
        while(indexNum1 >= 0 || indexNum2 >= 0){
            int x1 = indexNum1 >= 0 ? num1.charAt(indexNum1) - '0' : 0;
            int x2 = indexNum2 >= 0 ? num2.charAt(indexNum2) - '0' : 0;
            int sum = (x1+x2+carry)%10;
            carry = (x1+x2+carry)/10;
            result.append(sum);
            indexNum1 --;
            indexNum2 --;
        }

        if(carry!=0){
            result.append(carry);
        }

        return result.reverse().toString();
    }
}
