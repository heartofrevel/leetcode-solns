package root.Problem_7;

public class Solution {
    public int reverse(int x) {
        char[] num = String.valueOf(x).toCharArray();
        int left = 0;
        int right = num.length-1;
        int result;
        while(left < right){
            if(num[left] == '-'){
                left++;
                continue;
            }
            char temp = num[left];
            num[left] = num[right];
            num[right] = temp;
            left++;
            right--;
        }
        try{
            result = Integer.valueOf(String.valueOf(num));
        }catch (NumberFormatException e){
            result = 0;
        }
        return result;
    }
}
