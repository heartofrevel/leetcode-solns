package root.Problem_344;

public class Solution {
    public void reverseString(char[] s) {
        int len = s.length;
        for(int i=0,k=len-1;i<len;i++,k--){
            if(i>=k) break;
            char temp = s[i];
            s[i] = s[k];
            s[k] = temp;
        }
    }
}
