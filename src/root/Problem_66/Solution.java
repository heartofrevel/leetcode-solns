package root.Problem_66;

import java.util.Arrays;

public class Solution {
    public int[] plusOne(int[] digits) {
        int length = digits.length;
        int carry=0;
        for(int i=length-1; i>=0; i--){
            int sum = (digits[i]+1);
            carry = sum/10;
            digits[i] = sum%10;
            if(carry==0){
                break;
            }
        }
        if(carry!=0){
            digits = Arrays.copyOf(digits, length+1);
            digits[0]=1;
        }
        return digits;
    }
}
