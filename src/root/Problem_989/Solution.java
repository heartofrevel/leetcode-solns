package root.Problem_989;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Solution {
    /*
    Code with 67% efficiency
    public List<Integer> addToArrayForm(int[] num, int k) {
        List<Integer> result = new ArrayList<Integer>();
        String numToAdd = Integer.toString(k);
        int carry = 0;
        int lengthArray = num.length;
        int lengthNum = numToAdd.length();

        while(lengthArray>0 || lengthNum>0){
            int x = lengthNum>0 ? numToAdd.charAt(lengthNum-1) -'0' : 0;
            int y = lengthArray>0 ? num[lengthArray-1] : 0;
            int sum = (x + y + carry)%10;
            carry = (x + y + carry)/10;
            result.add(0, sum);
            lengthArray--;
            lengthNum--;
        }
        if(carry!=0)
            result.add(0, carry);
        return result;
    }
    */
    public List<Integer> addToArrayForm(int[] num, int k) {
        int length = num.length - 1;
        LinkedList<Integer> result = new LinkedList<>();
        while(length>=0 || k!=0){
            if(length >= 0)
                k+=num[length];
            result.addFirst(k%10);
            k/=10;
            length--;
        }
        return result;
    }


}
