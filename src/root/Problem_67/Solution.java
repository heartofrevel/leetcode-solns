package root.Problem_67;

public class Solution {
    public String addBinary(String a, String b) {
        int lenA = a.length()-1;
        int lenB = b.length()-1;
        StringBuilder result = new StringBuilder();
        int carry = 0;
        while(lenA>=0 || lenB>=0){
            int x = lenA>=0 ? a.charAt(lenA) - '0' : 0;
            int y = lenB>=0 ? b.charAt(lenB) - '0' : 0;
            int sum = x+y+carry;
            if(sum == 2){
                result.append('0');
                carry = 1;
            } else if(sum == 1){
                result.append('1');
                carry = 0;
            } else if(sum == 3){
                result.append('1');
                carry = 1;
            } else{
                result.append('0');
                carry = 0;
            }
            lenA--;
            lenB--;
        }
        if(carry==1){
            result.append('1');
        }

        return result.reverse().toString();
    }
}
